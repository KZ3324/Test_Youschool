<?php

namespace App\Repository;

use App\Command\AjoutLeadsBdd;
use App\Entity\Leads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Finder\Finder;

/**
 * @extends ServiceEntityRepository<Leads>
 *
 * @method Leads|null find($id, $lockMode = null, $lockVersion = null)
 * @method Leads|null findOneBy(array $criteria, array $orderBy = null)
 * @method Leads[]    findAll()
 * @method Leads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadsRepository extends ServiceEntityRepository
{
    private $csvOptions = [
        'finder_in' => 'public/leads',
        'finder_name' => 'test_ys_import.csv',
        'ignoreFirstLine' => true,
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Leads::class);
    }

    public function add(Leads $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Leads $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function addFichierCsv()
    {
        $conn = $this->getEntityManager()->getConnection();

        // Recherche du fichier csv dans le dossier nomée dans la variable privée
        $finder = new Finder();
        $finder
            ->files()
            ->in($this->csvOptions['finder_in'])
            ->name($this->csvOptions['finder_name']);

        foreach ($finder as $file) {
            $csv = $file;
        }

        if (($handle = fopen($csv->getRealPath(), 'r')) !== false) {
            fgetcsv($handle); // Enlever la première ligne du fichier CSV
            while (($data = fgetcsv($handle, null, ',')) !== false) {
                $data[5] = '+33 ' . $data[5]; // Ajouter le +33 devant le numéro

                $sql =
                    "INSERT into leads 
                (id, date_reception, nom, prenom, email, numero, source, is_client) 
                VALUES 
                ('" .
                    $data[0] .
                    "','" .
                    $data[1] .
                    "','" .
                    $data[2] .
                    "','" .
                    $data[3] .
                    "','" .
                    $data[4] .
                    "','" .
                    $data[5] .
                    "','" .
                    $data[6] .
                    "','" .
                    $data[7] .
                    "')";
                $stmt = $conn->prepare($sql);

                $stmt->executeQuery();

                if ($data[7] == true) {
                    $sql_client = "INSERT into client
                    (leads_id_id, created_at) VALUES ('$data[0]', NOW())
                    ";
                    $stmt_client = $conn->prepare($sql_client);
                    $stmt_client->executeQuery()->fetchAllAssociative();
                }
            }
            fclose($handle);
        }

        return 0;
    }

    //    /**
    //     * @return Leads[] Returns an array of Leads objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('l')
    //            ->andWhere('l.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('l.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Leads
    //    {
    //        return $this->createQueryBuilder('l')
    //            ->andWhere('l.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
