<?php

namespace App\Repository;

use App\Entity\CounterLeads;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CounterLeads>
 *
 * @method CounterLeads|null find($id, $lockMode = null, $lockVersion = null)
 * @method CounterLeads|null findOneBy(array $criteria, array $orderBy = null)
 * @method CounterLeads[]    findAll()
 * @method CounterLeads[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CounterLeadsRepository extends ServiceEntityRepository
{
    private $csvOptions = [
        'finder_in' => 'public/leads',
        'finder_name' => 'test_ys_import.csv',
        'ignoreFirstLine' => true,
    ];

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CounterLeads::class);
    }

    public function add(CounterLeads $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CounterLeads $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function countLeads()
    {
        $conn = $this->getEntityManager()->getConnection();

        // Select de toutes les sources disctinte
        $sql_source = 'SELECT DISTINCT(source) FROM leads';

        $stmt_source = $conn->prepare($sql_source);
        $result = $stmt_source->executeQuery()->fetchAllAssociative();
        

        foreach ($result as $key => $valeur) {
            // Select du nombre de source au total par client et du nombre total de leads par jour
            $count_total_sql = 'SELECT COUNT(id) FROM leads';
            $count_client_sql = "SELECT COUNT(source) FROM leads WHERE source = '$valeur[source]'";

            $stmt1 = $conn->prepare($count_total_sql);
            $stmt2 = $conn->prepare($count_client_sql);

            $result1 = $stmt1->executeQuery()->fetchAllAssociative();
            $result2 = $stmt2->executeQuery()->fetchAllAssociative();

            foreach ($result1 as $value) {
                $count_total = $value;
            }
            foreach ($result2 as $value) {
                $count_client = $value;
            }

            $sql_final =
                "INSERT INTO counter_leads
            (source, date, count_total, count_client)
            VALUES
            ('" .
                $valeur['source'] .
                "', NOW() , '" .
                $count_total['COUNT(id)'] .
                "', '" .
                $count_client['COUNT(source)'] .
                "');
            ";

            $stmt_final = $conn->prepare($sql_final);
            $stmt_final->executeQuery()->fetchAllAssociative();
        }
    }

    //    /**
    //     * @return CounterLeads[] Returns an array of CounterLeads objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?CounterLeads
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
