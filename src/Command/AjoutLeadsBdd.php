<?php

namespace App\Command;

use App\Repository\ClientRepository;
use App\Repository\CounterLeadsRepository;
use App\Repository\LeadsRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AjoutLeadsBdd extends Command
{
    private $leadsRepository;
    private $countLeads;

    protected static $defaultName = 'app:leads:add';

    public function __construct(LeadsRepository $leadsRepository, CounterLeadsRepository $counterLeads)
    {
        $this->leadsRepository = $leadsRepository;
        $this->countLeads = $counterLeads;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Ajouter tous les leads du ficher csv');
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $io = new SymfonyStyle($input, $output);

        $this->leadsRepository->addFichierCsv();
        $this->countLeads->countLeads();

        $io->success(sprintf('Ajout du fichier dans la base de données'));

        return 0;
    }
}
