<?php

namespace App\Entity;

use App\Repository\CounterLeadsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CounterLeadsRepository::class)]
class CounterLeads
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Source;

    #[ORM\Column(type: 'date')]
    private $Date;

    #[ORM\Column(type: 'string', length: 255)]
    private $count_total;

    #[ORM\Column(type: 'string', length: 255)]
    private $count_client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSource(): ?string
    {
        return $this->Source;
    }

    public function setSource(string $Source): self
    {
        $this->Source = $Source;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getCountTotal(): ?string
    {
        return $this->count_total;
    }

    public function setCountTotal(string $count_total): self
    {
        $this->count_total = $count_total;

        return $this;
    }

    public function getCountClient(): ?string
    {
        return $this->count_client;
    }

    public function setCountClient(string $count_client): self
    {
        $this->count_client = $count_client;

        return $this;
    }
}
